<?php

// BEGIN iThemes Security - Ne modifiez pas ou ne supprimez pas cette ligne
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Désactivez l’éditeur de code - iThemes Security > Réglages > Ajustements WordPress > Éditeur de code
// END iThemes Security - Ne modifiez pas ou ne supprimez pas cette ligne

define( 'ITSEC_ENCRYPTION_KEY', 'IH1ORHpLY1lrOEw4M3w3QiBIOTl3Z2BNaz5UMFtmVDxFenBmMnR+Zj1QZ2x+RC08MyR5UW5tclNbQmorX3hVWA==' );

/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'act-depannage' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S(By/u[Y6YYxE*|Ity)_41cJrdidQc.%S9`CQvy;<Hi0!msz%T{Vj8hboQJDIj:m' );
define( 'SECURE_AUTH_KEY',  '=`8wZjqG9XcOLA)v,5==O6h^E$_8c6{^+h<}9|]/ ;dhK=@>Vib;X0/G.;hQI S.' );
define( 'LOGGED_IN_KEY',    '1z6P0dpJ _U6v04^Cfoi#U)kE,yo@a$Qyjhg/:rFz7y-$w{q[435Vx9P>#9GQ*9p' );
define( 'NONCE_KEY',        't*?D;C =Voh$vq!3DH{P?@(T[BbuGRDeyat.aELknwV(_l< a0!bCg4Lo$nN6`U]' );
define( 'AUTH_SALT',        'nh=XH,>X,C)t~MO|Ye+HRIf?=pL<SRj6X-5=*OW?_m**<NyZ^#FCl^rS?U5+S{L]' );
define( 'SECURE_AUTH_SALT', 'VOC(mz[~F5;6PLG*2ZJ00Um~!NVYg)/ h^v#nSM*<7kr o FtA#cMyg2^Q|]}?V+' );
define( 'LOGGED_IN_SALT',   'ZW@N/z jqZ-.s@F&hbX 6& ^$ZAak{GsV%KjDYl[>s4kKn;[EbonmiK/D0?.@/[M' );
define( 'NONCE_SALT',       '`y7?_P{(Au.P^u.*3=Wu_0eY_^I1%t5Oy{%xGOz&zNDq!^<>r}z3NlL`}i#ZbA(&' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
